import { Component, OnInit } from '@angular/core';

import { Category } from '../classes/category';

import { MatDialog, MatDialogRef } from "@angular/material";
import { DialogComponent, DialogResult } from '../dialog/dialog.component';
import { EditDialogComponent } from '../editdialog/editdialog.component';
import { GlobalsService } from '../services/globals.service';
import { ProxyService } from '../services/proxy.service';
import { NetworkService } from '../services/network.service';
import { Type } from '../classes/type';

@Component({
  selector: 'groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  public categories: Array<Category>;
  private types: Array<Type>

  private showDialog(text: string, showYes: boolean = false, showNo: boolean = false, showCancel: boolean = false) : MatDialogRef<any> {
    let dialogRef = this.dialog.open(DialogComponent, {
        data: { text : text, show_yes: showYes, show_no: showNo, show_cancel: showCancel },
    });

      return dialogRef;
  }

  constructor(private netService: NetworkService, private globals: GlobalsService, 
    private proxy: ProxyService, private dialog: MatDialog) { }

  ngOnInit() {
    this.getTypeList();
    this.getCategories();
  }

  private getCategories() {
    this.proxy.getCategories().then(
      categories => {
        this.categories = categories.filter(x => true);
      }
    );
  }

  getTypeList() {
    this.proxy.getTypeList().then(result => {
      this.types = result;
    });
  }

  public editCategory(event, category: Category) {
    let dialogRef = this.dialog.open(EditDialogComponent, {
        data: { object : category, types: this.types, mode: 'edit' },
    });

    dialogRef.afterClosed().subscribe(result => {
      switch (result.action) {
        case 'save':
          this.saveCategory(result.object);
        break;

        case 'delete':
          this.deleteCategory(result.object);
        break;

        case 'cancel':
        default:
        break;
      };
    });
  }

  private deleteCategory(category: Category) {
    let dialogRef = this.showDialog("delete_group", true, true);
    dialogRef.afterClosed().subscribe(result => {
      if (DialogResult.RESULT_YES == result) {
          this.proxy.deleteCategory(category).then(result => {
            if (result) {
              this.getCategories();
            } else {
              this.showDialog("error_delete_group");
            }
          })
      }
    });
  }

  public createCategory() {
    let category = new Category();
    let dialogRef = this.dialog.open(EditDialogComponent, {
      data: { object : category, types: this.types, mode: 'create' },
    });

    dialogRef.afterClosed().subscribe(result => {
      if ('create' == result.action) {
        let category = result.object;
        this.proxy.createCategory(category).then(result => {
          if (result) {
            this.getCategories();
          } else {
            this.showDialog("error_creating_group");
          }
        });
      };
    });
  }

  private saveCategory(category: Category) {
    this.proxy.saveCategory(category).then(result => {
      if (result) {
        this.getCategories();
      } else {
        this.showDialog("error_saving_group");
      }
    }
    );
  }

}
