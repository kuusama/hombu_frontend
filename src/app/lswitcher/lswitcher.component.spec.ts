import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LswitcherComponent } from './lswitcher.component';

describe('LswitcherComponent', () => {
  let component: LswitcherComponent;
  let fixture: ComponentFixture<LswitcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LswitcherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LswitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
