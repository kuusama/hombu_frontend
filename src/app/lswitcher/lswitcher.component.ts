import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GlobalsService } from '../services/globals.service';
import { Language, SSettings } from '../../app/classes/settings';

@Component({
  selector: 'lswitcher',
  templateUrl: './lswitcher.component.html',
  styleUrls: ['./lswitcher.component.css']
})
export class LswitcherComponent implements OnInit {
  private currentSettings: SSettings;
  private languageKeys: Array<any>;   languages = Language;
  private langLiteral: Array<string> = ['dummy', 'en', 'ru', 'jp'];

  constructor(private globals: GlobalsService, private translate: TranslateService) {
    this.languageKeys  = Object.keys(this.languages).filter(Number);
  }

  ngOnInit() {
    this.currentSettings = this.globals.getCurrentSettings();
  }

  private writeToGlobalSettings() {
    this.globals.setCurrentSettings(this.currentSettings);
  }

  changeSelectedLang(lang: Language) {
    this.currentSettings.language = lang;
    this.writeToGlobalSettings();
    this.translate.use(this.langLiteral[lang]);
  }

}
