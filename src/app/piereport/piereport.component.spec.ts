import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiereportComponent } from './piereport.component';

describe('PiereportComponent', () => {
  let component: PiereportComponent;
  let fixture: ComponentFixture<PiereportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiereportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiereportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
