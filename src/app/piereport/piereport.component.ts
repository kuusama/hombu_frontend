import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BaseChartDirective } from 'ng2-charts';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { GetReportCommand, ReportItem, ReportTransaction, ReportType } from '../classes/command';
import { CommonReport, IntervalType } from '../common-report';
import { GlobalsService } from '../services/globals.service';
import { NetworkService } from '../services/network.service';
import { MatDialog } from "@angular/material";
import { DateDialogComponent } from '../datedialog/datedialog.component';
import { Account } from '../classes/account';
import { PickerComponent } from '../picker/picker.component';
import { ProxyService } from '../services/proxy.service';
import { TransactService } from '../services/transact.service';
import { Subscription } from 'rxjs';

declare var Chart: any;

@Component({
  selector: 'piereport',
  templateUrl: './piereport.component.html',
  styleUrls: ['./piereport.component.css']
})
export class PiereportComponent  extends CommonReport implements OnInit {
  @ViewChild(BaseChartDirective, {static: false}) chart: BaseChartDirective;

  tooltip: string = "";
  private refreshRef: Subscription = null;

  public accounts  : Array<Account>;
  private accountId : number;

  private currentReportItems: any[] = null;
  private transactions: Array<ReportTransaction> = null;
  private currentCategory: number = null;

  public chartTargetIncome : boolean = false;

  private report_title : string = "";

  public changeChartTarget() {
    this.chartTargetIncome = !this.chartTargetIncome;
    this.doRefresh();
  }

  // Doughnut
  public showChart = false;
  public doughnutChartLabels:string[];
  public doughnutChartData:number[]; 
  public doughnutChartType:string = 'doughnut';

  public chartOptions = {
    responsive: true,
    maintainAspectRatio: true,
    legend : {
      display: true,
      position : 'bottom',
      labels: {
        fontSize: (this.globals.getCurrentSettings().legendType == 1 ? 16 : 24),
        fontFamily : (this.globals.getCurrentSettings().legendType == 1 ? "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif" : 'Flaticon')
      },
    },
    tooltips :{
      bodyFontFamily : (this.globals.getCurrentSettings().legendType == 1 ? "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif" : 'Flaticon')
    },
    plugins: {
      datalabels: {
        backgroundColor: function(context) {
          return context.dataset.backgroundColor;
        },
        borderColor: 'white',
        borderRadius: 25,
        borderWidth: 2,
        color: 'black',
        anchor: 'center',
        align: 'center',
        offset: 0,
        display: function(context) {
          var dataset = context.dataset;
          var count = dataset.data.length;
          var value = dataset.data[context.dataIndex];
          return value > count * 1.5;
        },
        font: {
          weight: 'normal',
          size: '15'
        },
        formatter: Math.round
      }
    },
    elements: {
      center: {
          // the longest text that could appear in the center
          maxText: '9999999',
          text: '100500',
          textIncome: '12500',
          textBefore: '12500',
          textAfter: '555555',
          fontColor: '#FF5252',
          fontColorBefore: '#4b29df',
          fontColorAfter:  '#4b29df',
          fontColorIncome:  '#04c52a',
          fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
          fontStyle: 'normal',
          minFontSize: 1,
          maxFontSize: 256,
      }
  }
  };
 
  // events
  public chartClicked(e:any):void {
    if (e.active[0]) { 
      if (this.currentCategory != e.active[0]._index) {
        this.currentCategory =  e.active[0]._index;
        this.get_transacion_report(
          this.currentReportItems[e.active[0]._index].id,
          this.currentReportItems[e.active[0]._index].transfer,
          this.currentReportItems[e.active[0]._index].name
        );
      } else {
        this.currentCategory = null;
        this.transactions    = null;
      }
    }
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }

  constructor(private netService: NetworkService, private globals: GlobalsService,
      translate: TranslateService, private dialog: MatDialog,
      private transact: TransactService, private proxy: ProxyService) {
        super(translate);
  }

  ngOnInit() {
    this.refreshRef = this.transact.refresh$.subscribe(()=>{
      this.doRefresh();
    });

    Chart.pluginService.register({
      afterUpdate: function (chart) {
          if (chart.config.options.elements.center) {
              var helpers = Chart.helpers;
              var centerConfig = chart.config.options.elements.center;
              var globalConfig = Chart.defaults.global;
              var ctx = chart.chart.ctx;
  
              var fontStyle = helpers.getValueOrDefault(centerConfig.fontStyle, globalConfig.defaultFontStyle);
              var fontFamily = helpers.getValueOrDefault(centerConfig.fontFamily, globalConfig.defaultFontFamily);
  
              if (centerConfig.fontSize)
                  var fontSize = centerConfig.fontSize;
              // figure out the best font size, if one is not specified
              else {
                  ctx.save();
                  var fontSize = helpers.getValueOrDefault(centerConfig.minFontSize, 1);
                  var maxFontSize = helpers.getValueOrDefault(centerConfig.maxFontSize, 256);
                  var maxText = helpers.getValueOrDefault(centerConfig.maxText, centerConfig.text);
  
                  do {
                      ctx.font = helpers.fontString(fontSize, fontStyle, fontFamily);
                      var textWidth = ctx.measureText(maxText).width;
  
                      // check if it fits, is within configured limits and that we are not simply toggling back and forth
                      if (textWidth < chart.innerRadius * 2 && fontSize < maxFontSize)
                          fontSize += 1;
                      else {
                          // reverse last step
                          fontSize -= 1;
                          break;
                      }
                  } while (true)
                  ctx.restore();
              }
  
              // save properties
              chart.center = {
                  font_size: fontSize,
                  font: helpers.fontString(fontSize, fontStyle, fontFamily),
                  font_small: helpers.fontString(0.6 * fontSize, fontStyle, fontFamily),
                  fillStyle: helpers.getValueOrDefault(centerConfig.fontColor, globalConfig.defaultFontColor),
                  fillStyleBefore: helpers.getValueOrDefault(centerConfig.fontColorBefore, globalConfig.defaultFontColor),
                  fillStyleAfter: helpers.getValueOrDefault(centerConfig.fontColorAfter, globalConfig.defaultFontColor),
                  fillStyleIncome: helpers.getValueOrDefault(centerConfig.fontColorIncome, globalConfig.defaultFontColor)
              };
              chart.chart.options.plugins.datalabels.font.size = 0.5 * fontSize;
          } 
      },
      afterDraw: function (chart) {
          if (chart.center) {
              var centerConfig = chart.config.options.elements.center;
              var ctx = chart.chart.ctx;

              /* before */
              ctx.save();
              ctx.font = chart.center.font_small;
              ctx.fillStyle = chart.center.fillStyleBefore;
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';
              var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
              var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2 - chart.center.font_size;
              ctx.fillText(centerConfig.textBefore, centerX, centerY);
              ctx.restore();
              /* before - end */
  
              ctx.save();
              ctx.font = chart.center.font_small;
              ctx.fillStyle = chart.center.fillStyle;
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';
              var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
              var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2 + Number(chart.center.font_size * 0.6 ) / 2;
              ctx.fillText(centerConfig.text, centerX, centerY);
              ctx.restore();

              /* income */
              ctx.save();
              ctx.font = chart.center.font_small;
              ctx.fillStyle = chart.center.fillStyleIncome;
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';
              var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
              var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2 - Number(chart.center.font_size * 0.6) / 2;
              ctx.fillText(centerConfig.textIncome, centerX, centerY);
              ctx.restore();
              /* income - end */

              /* after */
              ctx.save();
              ctx.font = chart.center.font_small;
              ctx.fillStyle = chart.center.fillStyleAfter;
              ctx.textAlign = 'center';
              ctx.textBaseline = 'middle';
              var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
              var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2 + Number(chart.center.font_size);
              ctx.fillText(centerConfig.textAfter, centerX, centerY);
              ctx.restore();
              /* after - end */
          }
      },
    });

    this.proxy.getAccounts().then (
      accounts => {
        this.accounts = accounts;
        this.accounts[0] = { id: 0, name: this.translate.instant('ac_all'), currency: 0,
              icon: 1, curr_name : "zz", curr_sign: "Z"
        };

        this.tooltip = ( this.globals.getCurrentSettings().showTooltip == 1 ? "tooltip" : "");
        this.accountId = this.globals.getCurrentSettings().lastUsedSrcAccount;

        if (!this.accountId) {
          //Setup account if last used account is unknown
          this.accountId = this.accounts.filter(x => true)[0].id;
        }
  
        this.doInit();
        this.doRefresh();
      }
    )
  }

  ngOnDestroy() {
    this.refreshRef.unsubscribe();
  }

  public doRefresh() {
    this.currentCategory = null;
    this.transactions    = null;
    this.showChart = false;
    let date_begin = new Date(this.currentDate);
    let date_end = new Date( (this.intervalType == IntervalType.OneDay) ? this.currentDate : this.endDate);
    this.endDate = date_end;

    date_begin.setHours(0,0,0,0);
    //this.beginTimestamp = Math.round( date_begin.getTime()/1000 );
    let offset = date_begin.getTimezoneOffset();
    this.beginTimestamp = Math.round(date_begin.getTime() / 1000) - (60 * offset);

    date_end.setHours(23,59,59,999);
//    this.endTimestamp = Math.round( date_end.getTime()/1000 );
    this.endTimestamp = Math.round(date_end.getTime() / 1000) - (60 * offset);

    Cookie.set('rep_date_begin', date_begin.toDateString(), 10);
    Cookie.set('rep_date_end'  , date_end.toDateString(), 10);
    Cookie.set('rep_date_interval'  , this.intervalType.toString(), 10);

    let comm = new GetReportCommand(this.globals.getCurrentSettings().userid, 
      this.beginTimestamp, this.endTimestamp, ReportType.PIE_ACCOUNT,
      this.accountId , 0,
      this.globals.getCurrentSettings().imode);
    
    this.netService.sendCommand(comm).then(result => {
      this.currentDateInterval = ( (this.intervalType == IntervalType.OneDay) ?
              this.formatDate(date_begin) :
              this.formatDate(date_begin) + " - " + this.formatDate(date_end));

      let doughnutCharLabels = new Array<string>();
      let doughnutChartData  = new Array<number>();
      this.currentReportItems = new Array<any>();
      let total : number = 0;
      let total_income : number = 0;
      let total_before : number = 0;
      let total_after : number = 0;

      if (this.chartTargetIncome) {
        JSON.parse(result).forEach(element => {
          if (1 == element.otype) {
            this.currentReportItems.push(element);
            let label = (this.globals.getCurrentSettings().legendType == 1 ? element.name : 
              String.fromCharCode(parseInt(this.iconMap.get(element.icon), 16))
            );
            doughnutCharLabels.push(label);
            doughnutChartData.push(element.income);
            total_income += Number(element.income);
          } else if (2 == element.otype) {
            total += Number(element.expense);
          }else if (4 == element.otype) {
            total_before = element.income;
            total_after  = element.expense;
          }
        });

      } else {
        JSON.parse(result).forEach(element => {
          if (2 == element.otype) {
            this.currentReportItems.push(element);
            let charCode = this.iconMap.get(String(element.icon));
            let label = (this.globals.getCurrentSettings().legendType == 1 ? element.name : 
              String.fromCharCode(parseInt(this.iconMap.get(element.icon), 16))
            );
            doughnutCharLabels.push(label);
            doughnutChartData.push(element.expense);
            total += Number(element.expense);
          } else if (1 == element.otype) {
            total_income += Number(element.income);
          }else if (4 == element.otype) {
            total_before = element.income;
            total_after  = element.expense;
          }
        });
      }

      this.doughnutChartLabels = doughnutCharLabels;
      this.doughnutChartData   = doughnutChartData;
      this.chartOptions.elements.center.text = total.toString();
      this.chartOptions.elements.center.textIncome = total_income.toString();
      this.chartOptions.elements.center.textBefore = total_before.toString();
      this.chartOptions.elements.center.textAfter = total_after.toString();
      this.showChart = true;
    }
    )
  }

  public get_category_report(reportItem : ReportItem) {
    if(0 != reportItem.id) {
      if (!reportItem.subItems){
        let comm = new GetReportCommand(this.globals.getCurrentSettings().userid, 
          this.beginTimestamp, this.endTimestamp, ReportType.ACCOUNT_REPORT, reportItem.id, 0,
          this.globals.getCurrentSettings().imode);
        
          this.netService.sendCommand(comm).then(result => {
            reportItem.subItems = JSON.parse(result);
            reportItem.subItems.forEach(s => {
              s.account = reportItem.id;
              s.style = ( (1 == s.otype) ? 'income' : (2 == s.otype ? 'expense' : 'transfer') );
            });
          });
      } else {
        reportItem.subItems = null;
      }
    }
  }

  public get_transacion_report(category: number, transfer: number, header: string) {
    if (1 == transfer) {
      category = 0;
    }
    let comm = new GetReportCommand(this.globals.getCurrentSettings().userid, 
      this.beginTimestamp, this.endTimestamp, ReportType.TRANSACTION_REPORT,
        this.accountId,
        category,
      this.globals.getCurrentSettings().imode);
    
      this.netService.sendCommand(comm).then(result => {
        let tmpArr : Array<ReportTransaction> = JSON.parse(result);
        tmpArr.forEach(t => {
          t.otype = 'income';

          if (1 == transfer) {
            t.otype = 'transfer';
          } else if(t.expense > 0) {
            t.otype = 'expense';
          }

        if (null == t.date) {
            var index = tmpArr.indexOf(t, 0);
            if (index > -1) {
              tmpArr.splice(index, 1);
            }
          }
        });
        this.transactions = tmpArr;
        this.report_title = header;
      });
  }

  public switchDateSelector() {
    let dialogRef = this.dialog.open(DateDialogComponent, {
      data: {
        dateBegin: this.currentDate,
        dateEnd: this.endDate,
        iType: this.intervalType 
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.currentDate = result.dateBegin;
        this.endDate = result.dateEnd;
        this.intervalType = result.iType;
        this.doRefresh();
      }
    });
  }

  private edit_transaction(trans : ReportTransaction) {
    this.transact.editTransaction(trans.id);
  }

  private showPicker(data: any) : any {
    let dialogRef = this.dialog.open(PickerComponent, {
      data: data.filter(function( element ) { return element !== undefined; }),
    });
    return dialogRef;
  }

  private changeAccount(): void {
    let dialogRef = this.showPicker(this.accounts);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.accountId = result;
        if (result > 0) {
          this.globals.setLastUsedSrcAccount(result);
        }
        this.doRefresh();
      }
    });
  }

}
