import { TranslateService } from '@ngx-translate/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { ReportItem } from './classes/command';

export enum IntervalType {
    OneDay   = 1,
    Interval = 2
}

export enum TimeIntervalType {
    OneDay   = 1,
    OneWeek  = 2,
    OneMonth = 3,
    OneYear  = 4
}

export abstract class CommonReport {
    translate : TranslateService;
    localeName: string = "ru-RU";

    public currentReport  : Array<ReportItem>;
    currentDate    : Date;
    endDate        : Date;
    currentDateString : string;
    endDateString     : string;
    intervalType   : IntervalType;
    currentDateInterval : string;
    beginTimestamp : number;
    endTimestamp   : number;

    IntervalType = IntervalType;
    IntervalTypeValues = Object.values(IntervalType).filter(e => typeof(e)=="number");

    protected iconMap = new Map([
        ["1", "f10f"], ["2", "f110"], ["3", "f111"], ["4", "f112"], ["5", "f114"], ["6", "f115"],
        ["7", "f11a"], ["8", "f113"], ["9", "f15c"], ["10", "f153"], ["11", "f168"], ["12", "f16d"],
        ["13", "f117"], ["14", "f16f"], ["15", "f121"], ["16", "f13a"], ["17", "f13b"], ["18", "f199"],
        ["19", "f11b"], ["20", "f18c"], ["21", "f165"], ["22", "f118"], ["23", "f119"], ["24", "f15d"],
        ["25", "f15e"], ["26", "f103"], ["27", "f11d"], ["28", "f11f"], ["29", "f15a"], ["30", "f1a3"],
        ["31", "f105"], ["32", "f11e"], ["33", "f123"], ["34", "f183"], ["35", "f142"], ["36", "f15b"],
        ["37", "f170"], ["38", "f194"], ["39", "f18e"], ["40", "f122"], ["41", "f192"], ["42", "f13c"],
        ["43", "f157"], ["44", "f188"], ["45", "f161"], ["46", "f181"], ["47", "f169"], ["48", "f18f"],
        ["49", "f16e"], ["50", "f17e"], ["51", "f107"], ["52", "f13d"], ["53", "f189"], ["54", "f141"],
        ["55", "f125"], ["56", "f140"], ["57", "f190"], ["58", "f171"], ["59", "f187"], ["60", "f10b"],
        ["61", "f124"], ["62", "f166"], ["63", "f19e"], ["64", "f10a"], ["65", "f185"], ["66", "f12d"],
        ["67", "f17c"], ["68", "f17f"], ["69", "f198"], ["70", "f17d"], ["71", "f191"], ["72", "f10c"],
        ["73", "f19f"], ["74", "f134"], ["75", "f136"], ["76", "f137"], ["77", "f138"], ["78", "f13e"],
        ["79", "f132"], ["80", "f133"], ["81", "f135"], ["82", "f139"], ["83", "f116"], ["84", "f12f"],
        ["85", "f12e"], ["86", "f130"], ["87", "f131"], ["88", "f148"], ["89", "f143"], ["90", "f144"],
        ["91", "f145"], ["92", "f146"], ["93", "f147"], ["94", "f149"], ["95", "f14a"], ["96", "f14b"],
        ["97", "f14c"], ["98", "f14d"], ["99", "f14e"], ["100", "f150"], ["101", "f151"], ["102", "f152"],
        ["103", "f14f"], ["104", "f180"], ["105", "f164"], ["106", "f167"], ["107", "f155"], ["108", "f16a"],
        ["109", "f120"], ["110", "f126"], ["111", "f156"], ["112", "f160"], ["113", "f175"], ["114", "f108"],
        ["115", "f106"], ["116", "f109"], ["117", "f172"], ["118", "f158"], ["119", "f173"], ["120", "f193"],
        ["121", "f182"], ["122", "f184"], ["123", "f18d"], ["124", "f1a6"], ["125", "f12c"], ["126", "f195"],
        ["127", "f18a"], ["128", "f196"], ["129", "f17b"], ["130", "f18b"], ["131", "f1a4"], ["132", "f154"],
        ["133", "f16c"], ["134", "f197"], ["135", "f159"], ["136", "f1a0"], ["137", "f179"], ["138", "f178"],
        ["139", "f12a"], ["140", "f12b"], ["141", "f11c"], ["142", "f15f"], ["143", "f13f"], ["144", "f162"],
        ["145", "f163"], ["146", "f174"], ["147", "f17a"], ["148", "f104"]
    ]);

    constructor(translate: TranslateService) {
        this.translate = translate;
    }
    doInit() {
        var dtBegin    : string = null;
        var dtEnd      : string = null;
        var dtInterval : string = null;

        let cookielist = Cookie.getAll();
        if (null != cookielist) {
            dtBegin    = cookielist.rep_date_begin;
            dtEnd      = cookielist.rep_date_end;
            dtInterval = cookielist.rep_date_interval;
        }

        this.currentDate   = ((dtBegin == null) ? new Date() : new Date(dtBegin));
        this.endDate       = ((dtEnd   == null) ? new Date() : new Date(dtEnd));
        this.currentReport = null;
        this.intervalType  = ((dtInterval == null) ? IntervalType.OneDay : Number(dtInterval));
        this.currentDateInterval = this.formatDate(this.currentDate);
        this.setDateFormats();
    }

    private setDateFormats() {
            this.translate.get("localeName", {}).subscribe((res: string) => {
            this.localeName = res;
        });
    }

    protected formatDate(dat: Date) {
        return dat.toLocaleDateString(this.localeName);
    }

    protected abstract doRefresh();

    public moveDate(arg: number) {
        let diff : number = 0;
        if (this.intervalType === IntervalType.Interval) {
            // if(!this.currentDate.getTime) {
            //     this.currentDate = this.currentDate._d;
            // }

            //Check if current interval is one month
            var firstDay = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), 1);
            var lastDay = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1, 0);
            lastDay.setHours(23,59,59,999);

            if (firstDay.getTime() == this.currentDate.getTime() && lastDay.getTime() == this.endDate.getTime()) {
                if (this.currentDate.getMonth() == 11 && arg > 0) {
                    this.currentDate = new Date(this.currentDate.getFullYear() + 1, 0, 1);
                } else {
                    this.currentDate = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1 * arg, 1);
                }
                this.endDate = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth() + 1, 0);

            } else {
                diff = arg * (this.endDate.getTime() - this.currentDate.getTime());
                this.endDate = new Date(this.endDate.getTime() + diff);
            }

        } else {
            //Just add one day
            diff = arg * 60*60*24*1000;
        }
        this.currentDate = new Date(this.currentDate.getTime() + diff);
        this.doRefresh();
    }
}