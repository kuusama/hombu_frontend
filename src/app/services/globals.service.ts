import { Injectable } from '@angular/core';

import { GetSettingsCommand, SetSettingsCommand } from '../classes/command';
import { NetworkService } from './network.service';
import { SSettings } from '../classes/settings';

@Injectable()
export class GlobalsService {

  static currentSettings : SSettings;

  constructor(private netService: NetworkService) {}

  public setCurrentSettings(settings: SSettings) {
    GlobalsService.currentSettings = settings;
  }

  public getCurrentSettings() {
    if (!GlobalsService.currentSettings) {
      GlobalsService.currentSettings = new SSettings(0);
    }
    return GlobalsService.currentSettings;
  }

  public downloadUserSettings(userId: number) : Promise<SSettings> {
    let command = new GetSettingsCommand(userId);
    let promResult = false;
    return this.netService.sendCommand(command).then(
        result => {
          if ('0' != result) {
            let resSettings: SSettings = JSON.parse(result);
            GlobalsService.currentSettings = resSettings;
            return new Promise<SSettings>((resolve) => {
              resolve (resSettings);
            });
          }
        }
    ).catch(this.handleError);
  }

  public uploadUserSettings() : Promise<string> {
    let command = new SetSettingsCommand(GlobalsService.currentSettings);
    return this.netService.sendCommand(command).then(
        result => {
          return result;
        }
    )
  }

  public downloadExchangeRates() : Promise<any> {
    const url = 'https://www.cbr-xml-daily.ru/daily_json.js'
    return this.netService.getExternalResourse(url).then(
      result => {
        return result["Valute"];
      }
    )
  }

  private handleError (error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

  setLastUsedSrcAccount(accountId: number) {
    GlobalsService.currentSettings.lastUsedSrcAccount = accountId;
  }

  setLastUsedTgtAccount(accountId: number) {
    GlobalsService.currentSettings.lastUsedTgtAccount = accountId;
  }

  setLastUsedIncomeCategory(categoryId: number) {
    GlobalsService.currentSettings.lastUsedIncomeCategory = categoryId;
  }

  setLastUsedOutcomeCategory(categoryId: number) {
    GlobalsService.currentSettings.lastUsedOutcomeCategory = categoryId;
  }

}