import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';

import { Command } from '../classes/command';

@Injectable()
export class NetworkService {
    //private backendUrl : string  = "/backend/index.php";
    private backendUrl : string  = "http://127.0.0.1/litemoney/index.php";
    public sendCommand(loginCommand: Command ) : Promise<string> {
        let headers = new Headers({'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'});
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.backendUrl, 
        "command=" + JSON.stringify(loginCommand),
         options).toPromise()
               .then( (res : Response) => {
                    let body = res.json();
                    return body.data;
                }
                )
                .catch(this.handleError);
    }

    public getExternalResourse(externalUrl: string) : Promise<any> {
        return this.http.get(externalUrl).toPromise().then(
            (res : Response) => {
                if (res && 200 == res.status) {
                    return res.json();
                }
            }
        )
        .catch(this.handleError);
    }

    private handleError (error: Response | any) {
        console.error(error.message || error);
        return Promise.reject(error.message || error);
    }

    constructor(private http: Http) { }
}