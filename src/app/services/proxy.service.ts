import { Injectable } from '@angular/core';

import { NetworkService } from './network.service';
import { GlobalsService } from './globals.service';

import { Account }  from '../classes/account';
import { Category } from '../classes/category';
import { Currency } from '../classes/currency';
import { Type }     from '../classes/type';
import {
  GetAccountsCommand, SaveAccountCommand, CreateAccountCommand, DeleteAccountCommand,
  GetCategoriesCommand, SaveCategoryCommand, CreateCategoryCommand, DeleteCategoryCommand,
  GetCurrencyListCommand, GetTypeListCommand
} from '../classes/command';

@Injectable()
export class ProxyService {

  static categories: Array<Category>;
  static accounts  : Array<Account>;
  static currencies: Array<Currency>;
  static types     : Array<Type>

  constructor(private netService: NetworkService, private globals : GlobalsService ) { }

  public getAccounts(): Promise<Array<Account>> {
    if (ProxyService.accounts && 0 != ProxyService.accounts.length) {
      return new Promise<Array<Account>>((resolve) => {
        resolve(ProxyService.accounts);
      });
    } else {
      return new Promise<Array<Account>>((resolve) => {
        let comm = new GetAccountsCommand(this.globals.getCurrentSettings().userid);
        this.netService.sendCommand(comm)
          .then(result => {
              ProxyService.accounts = [];
              let accounts = JSON.parse(result);
              accounts.forEach(account => {
                ProxyService.accounts[account.id] = account;
              });
              resolve(ProxyService.accounts);
          })
      });
    }
  }

  public getCategories(): Promise<Array<Category>> {
    if (ProxyService.categories && 0 != ProxyService.categories.length) {
      return new Promise<Array<Category>>((resolve) => {
        resolve(ProxyService.categories);
      });
    } else {
      return new Promise<Array<Category>>((resolve) => {
        let comm = new GetCategoriesCommand(this.globals.getCurrentSettings().userid);
        this.netService.sendCommand(comm)
          .then(result => {
              ProxyService.categories = [];
              let categories = JSON.parse(result);
              categories.forEach(category => {
                ProxyService.categories[category.id] = category;
              });
              resolve(ProxyService.categories);
          })
      });
    }
  }

  public saveAccount(account: Account): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      let comm = new SaveAccountCommand(this.globals.getCurrentSettings().userid, account);
      this.netService.sendCommand(comm)
        .then(result => {
          if ('0' != result) {
            ProxyService.accounts[account.id] = account;
          }
          resolve (result != '0');
        }
        )
    })
  }

  public saveCategory(category: Category): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      let comm = new SaveCategoryCommand(this.globals.getCurrentSettings().userid, category);
      this.netService.sendCommand(comm)
        .then(result => {
          if ('0' != result) {
            ProxyService.categories[category.id] = category;
          }
          resolve (result != '0');
        }
        )
    })
  }

  public createAccount(account: Account): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      let comm = new CreateAccountCommand(this.globals.getCurrentSettings().userid, account);
      this.netService.sendCommand(comm)
        .then(result => {
          if ('0' != result) {
            account.id = Number(result);
            ProxyService.accounts[account.id] = account;
          } 
          resolve (result != '0');
        }
        );
    })
  }

  public createCategory(category: Category): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      let comm = new CreateCategoryCommand(this.globals.getCurrentSettings().userid, category);
      this.netService.sendCommand(comm)
        .then(result => {
          if ('0' != result) {
            category.id = Number(result);
            ProxyService.categories[category.id] = category;
          } 
          resolve (result != '0');
        }
        );
    })
  }

  public deleteAccount(account: Account): Promise<boolean> {
    return new Promise<boolean>((resolve) => {    
      let comm = new DeleteAccountCommand(this.globals.getCurrentSettings().userid, account);
      this.netService.sendCommand(comm)
        .then(result => {
          if ('0' != result) {
            const index = ProxyService.accounts.indexOf(account, 0);
            if (index > -1) {
              ProxyService.accounts.splice(index, 1);
            }
            resolve (true);
          } else {
            resolve (false);
          }
        }
        )
    })
  }

  public deleteCategory(category: Category): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      let comm = new DeleteCategoryCommand(this.globals.getCurrentSettings().userid, category);
      this.netService.sendCommand(comm)
        .then(result => {
          if ('0' != result) {
            const index = ProxyService.categories.indexOf(category, 0);
            if (index > -1) {
              ProxyService.categories.splice(index, 1);
            }
            resolve (true);
          } else {
            resolve (false);
          }
        }
        )
    })
  }

  public getCurrencyList(): Promise<Array<Currency>> {
    if (ProxyService.currencies && 0 != ProxyService.currencies.length) {
      return new Promise<Array<Currency>>((resolve) => {
        resolve(ProxyService.currencies);
      });
    } else {
      return new Promise<Array<Currency>>((resolve) => {
        let comm = new GetCurrencyListCommand(this.globals.getCurrentSettings().userid);
        this.netService.sendCommand(comm)
          .then(result => {
              ProxyService.currencies = JSON.parse(result);
              resolve(ProxyService.currencies);
          })
      });
    }
  }

  public getTypeList(): Promise<Array<Type>> {
    if (ProxyService.types && 0 != ProxyService.types.length) {
      return new Promise<Array<Type>>((resolve) => {
        resolve(ProxyService.types);
      });
    } else {
      return new Promise<Array<Type>>((resolve) => {
        let comm = new GetTypeListCommand(this.globals.getCurrentSettings().userid);
        this.netService.sendCommand(comm)
          .then(result => {
              ProxyService.types = JSON.parse(result);
              resolve(ProxyService.types);
          })
      });
    }
  }

  private handleError (error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }
}
