import { Injectable } from '@angular/core';
import { NetworkService } from './network.service';
import { GlobalsService } from './globals.service';
import { GetSettingsCommand, SetSettingsCommand } from '../classes/command';
import { SSettings } from '../classes/settings'

@Injectable()
export class SettingsService {
  constructor(private netService: NetworkService, private globals: GlobalsService) { }

  public getUserSettings(userId: number) : Promise<boolean> {
    let command = new GetSettingsCommand(userId);
    let promResult = false;
    this.netService.sendCommand(command).then(
        result => {
          if ('0' != result) {
            let resSettings: SSettings = JSON.parse(result);
            this.globals.setCurrentSettings(resSettings);
            promResult = true;
          }

        }
    ).catch(this.handleError);
    return new Promise<boolean>(resolve => {return promResult});
}

public saveSettings() : Promise<boolean> {
  let command = new SetSettingsCommand(this.globals.getCurrentSettings());
  let promResult = false;
  this.netService.sendCommand(command).then(
      result => {
        promResult = ('1' == result);
      }
  )
  return new Promise<boolean>(resolve => {return promResult});
}

private handleError (error: Response | any) {
  console.error(error.message || error);
  return Promise.reject(error.message || error);
}

}
