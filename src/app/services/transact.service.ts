import { Injectable } from '@angular/core';

import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Subject, Observable } from 'rxjs';
import { MatDialog } from "@angular/material";
import { TranslateService } from '@ngx-translate/core';

import {
  ConfirmTransactionCommand,
  DeleteTransactionCommand,
  EditTransactionCommand
} from '../classes/command';

import { UseDate } from '../classes/settings';
import { DialogComponent, DialogResult } from '../dialog/dialog.component';
import { CalculatorComponent } from '../calculator/calculator.component';
import { GlobalsService } from './globals.service';
import { NetworkService } from './network.service';
import { Transaction, TransactionType } from '../classes/transaction';


@Injectable()
export class TransactService {
  private refreshSource = new Subject<void>();
  public refresh$: Observable<void> = this.refreshSource.asObservable();

  private lastUsedDate: Date = new Date();
  isExtraSmall: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.XSmall);

  constructor(private netService: NetworkService, private globals: GlobalsService, 
    private translate: TranslateService, private dialog: MatDialog,
    private breakpointObserver: BreakpointObserver ) { }


  public newTransaction(type: TransactionType) {
    let trans = new Transaction(type);
    let settings = this.globals.getCurrentSettings();

    trans.date = (UseDate.LastUsedDate == settings.useDate ?
      this.lastUsedDate : new Date());
    trans.sourceAccount = settings.lastUsedSrcAccount;

    if (type == TransactionType.TRANSFER) {
      trans.targetAccount = (settings.lastUsedTgtAccount ? settings.lastUsedTgtAccount : settings.lastUsedSrcAccount);
    }

    trans.category = (TransactionType.INCOME == type) ? settings.lastUsedIncomeCategory : settings.lastUsedOutcomeCategory;

    this.changeTransaction(trans);
  }

  private changeTransaction(trans: Transaction) {
    let dialogRef = this.dialog.open(CalculatorComponent, {
      data: {
        transaction : trans
      },
      maxWidth: '100wv',
    });

    const smallDialogSubscription = this.isExtraSmall.subscribe(size => {
      if (size.matches) {
        dialogRef.updateSize('100%', '100%');
      } else {
        dialogRef.updateSize('unset', 'unset');
      }
    });

    dialogRef.afterClosed().subscribe(answer => {
      smallDialogSubscription.unsubscribe();
      if(answer && answer.result) {
        switch (answer.result) {
          case DialogResult.RESULT_CONFIRM:
            this.confirmTransaction(answer.trans);
          break;

          case DialogResult.RESULT_DELETE:
            this.deleteTransaction(answer.trans);
          break;
        }
      }
    });
  }

  private showDialog(text: string) : void {
    let dialogRef = this.dialog.open(DialogComponent, {
      data: { text : text },
    });
  }

  public confirmTransaction(trans: Transaction) {
    this.lastUsedDate = trans.date;
    let comm = new ConfirmTransactionCommand(this.globals.getCurrentSettings().userid, trans);
    this.netService.sendCommand(comm)
    .then(result => {
        let boolresult = ('0' != result);
        if (boolresult) {
          this.globals.uploadUserSettings();
          this.refreshSource.next();
        } else {
          this.showDialog("error_saving_transaction");
        }
    }
    )
  }

  public editTransaction(transId : number) {
    let comm = new EditTransactionCommand(this.globals.getCurrentSettings().userid, transId);
    this.netService.sendCommand(comm)
    .then(result => {
      let trans : Transaction = JSON.parse(result)[0];
      this.changeTransaction(trans);
    })
  }

  public deleteTransaction(trans: Transaction) {
    let comm = new DeleteTransactionCommand(this.globals.getCurrentSettings().userid, trans);
    this.netService.sendCommand(comm)
    .then(result => {

    })
  }
}