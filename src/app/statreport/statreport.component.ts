import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BaseChartDirective } from 'ng2-charts';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { GetReportCommand, ReportTransaction, ReportType } from '../classes/command';
import { CommonReport, TimeIntervalType } from '../common-report';
import { GlobalsService } from '../services/globals.service';
import { NetworkService } from '../services/network.service';
import { MatDialog } from "@angular/material";
import { DateDialogComponent } from '../datedialog/datedialog.component';

declare var Chart: any;

@Component({
  selector: 'statreport',
  templateUrl: './statreport.component.html',
  styleUrls: ['./statreport.component.css']
})
export class StatreportComponent extends CommonReport implements OnInit {
  @ViewChild(BaseChartDirective, {static: false}) chart: BaseChartDirective
  @Output() changeAccount = new EventEmitter<number>();

  tooltip: string = "";

  TimeIntervalType = TimeIntervalType;
  private TimeIntervalTypeValues = Object.values(TimeIntervalType).filter(e => typeof(e)=="number");
  timeIntervalType   : TimeIntervalType = TimeIntervalType.OneWeek; 

  private currentReportItems: any[] = null;
  private transactions: Array<ReportTransaction> = null;
  private currentCategory: number = null;

  public chartTargetIncome  : boolean = false;
  public chartTargetIsBalance : boolean = true;
  public chartBalancesCombined : boolean = true;

  private changeChartTarget() {
    this.chartTargetIncome = !this.chartTargetIncome;
    this.doRefresh();
  }

  public changeChartTargetBalance() {
    this.chartTargetIsBalance = !this.chartTargetIsBalance;
    this.doRefresh();
  }

  public changeBalancesCombined() {
    this.chartBalancesCombined = !this.chartBalancesCombined;
    this.doRefresh();
  }

// lineChart
public showChart = true;
public lineChartData : Array<any> = [];
public lineChartLabels : Array<any> = [];

public lineChartOptions : any = {
  responsive: true,
  maintainAspectRatio: false,
  legend : {
    display: true,
    position : 'bottom',
    labels: {
      fontSize: 14
    // fontSize: (this.globals.getCurrentSettings().legendType == 1 ? 16 : 24),
    // fontFamily : (this.globals.getCurrentSettings().legendType == 1 ? "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif" : 'Flaticon')
    },
  },
  tooltips :{
   // bodyFontFamily : (this.globals.getCurrentSettings().legendType == 1 ? "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif" : 'Flaticon')
  },
  plugins: {
    datalabels: {
      display: false
    }
  },
  animation: {
    duration: 0, // general animation time
  },
  scales: {
    yAxes: [{
        stacked: true
    }]
  }
};

public lineChartLegend:boolean = true;
public lineChartType:string = 'line';

  private emitChangeAccount() {
    this.changeAccount.emit(1);
  }


  constructor(private netService: NetworkService, private globals: GlobalsService,
    translate: TranslateService, private dialog: MatDialog) {
      super(translate);
  }

  ngOnInit() {
    this.tooltip = ( this.globals.getCurrentSettings().showTooltip == 1 ? "tooltip" : "");
    this.doInit();
    this.doRefresh();
  }

  public doRefresh() {
    this.currentCategory = null;
    this.transactions    = null;
    this.showChart = false;
    let date_begin = new Date(this.currentDate);
    let date_end = new Date(this.endDate);
    this.endDate = date_end;
  
    date_begin.setHours(0,0,0,0);
    //this.beginTimestamp = Math.round( date_begin.getTime()/1000 );
    let offset = date_begin.getTimezoneOffset();
    this.beginTimestamp = Math.round(date_begin.getTime() / 1000) - (60 * offset);

    date_end.setHours(23,59,59,999);
    //this.endTimestamp = Math.round( date_end.getTime()/1000 );
    this.endTimestamp = Math.round(date_end.getTime() / 1000) - (60 * offset);

    Cookie.set('rep_date_begin', date_begin.toDateString(), 10);
    Cookie.set('rep_date_end'  , date_end.toDateString(), 10);

    var comm : GetReportCommand;
    var reportType : ReportType;

    if (this.chartTargetIsBalance) {
      reportType = (this.chartBalancesCombined ? ReportType.BALANCES_COMBINED : ReportType.BALANCES_DETAIL);
    } else {
      if(this.chartBalancesCombined) {
        reportType = (this.chartTargetIncome ? ReportType.INCOME_COMBINED : ReportType.EXPENSE_COMBINED );
      } else {
        reportType = (this.chartTargetIncome ? ReportType.INCOME_DETAIL : ReportType.EXPENSE_DETAIL );
      }
    }

    comm = new GetReportCommand(this.globals.getCurrentSettings().userid, this.beginTimestamp, this.endTimestamp,
      reportType, 0, 0, this.timeIntervalType);

    this.netService.sendCommand(comm).then(result => {
      this.currentDateInterval = (this.formatDate(date_begin) + " - " + this.formatDate(date_end));

      this.lineChartLabels = new Array<string>();
      this.lineChartData   = new Array<any>();

      if (this.chartTargetIsBalance && this.chartBalancesCombined) {
        let data = new Array<number>();
        JSON.parse(result).data.forEach(point => {
          this.lineChartLabels.push(point.date);
          data.push(point.balance);
        });
        this.translate.get('ac_total', {value: 'ac_total'}).subscribe((res: string) => {
          this.lineChartData.push({data: data, label: res, lineTension: 0,});
        });
      } else if(this.chartTargetIsBalance && !this.chartBalancesCombined) {
        let resArray : Array<any> = JSON.parse(result).data;
        var i, j : number;
        j = -1;
        var currentAccount: string = "";
        resArray.forEach(element => {
          if (element.account != currentAccount) {
            currentAccount = element.account;
            j++;
            this.lineChartData.push({data: [], label: element.account, lineTension: 0, fill: (0 == j ? 'origin' : '-1') });
          }
          if (!this.lineChartLabels.includes(element.date)) {
            this.lineChartLabels.push(element.date);
          }
          this.lineChartData[j].data.push(element.balance);
          });
        } else if(!this.chartTargetIsBalance) {
          let resArray : any = result;
          
          let i = 0;
          resArray['cat'].forEach(category => {
            if (category === 'ac_total') {
              this.translate.get('ac_total', {value: 'ac_total'}).subscribe((res: string) => {
                this.lineChartData.push({data: resArray['incomes'][i] , label: res, lineTension: 0, fill: (0 == i ? 'origin' : '-1') });
              });
            } else {
              if (this.checkForNonZero(resArray['incomes'][i])) {
                this.lineChartData.push({data: resArray['incomes'][i] , label: category, lineTension: 0, fill: (0 == i ? 'origin' : '-1') });
              }
            }
            i++;
          });
          this.lineChartLabels = resArray['dates'];
        };

    this.showChart = true;
    });
  }

  private checkForNonZero (arr: Array<number>) : boolean {
    var result = false;
    arr.forEach(element => {
      if (element != 0) {
        result = true;
        return result;
      }
    });
    return result;
  }

  public switchDateSelector() {
    let dialogRef = this.dialog.open(DateDialogComponent, {
      data: {
        dateBegin: this.currentDate,
        dateEnd: this.endDate,
        iType: this.timeIntervalType,
        isStatReport: true 
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.currentDate = result.dateBegin;
        this.endDate = result.dateEnd;
        this.timeIntervalType = result.iType;
        this.doRefresh();
      }
    });
  }

}