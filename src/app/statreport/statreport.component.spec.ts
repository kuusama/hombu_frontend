import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatreportComponent } from './statreport.component';

describe('StatreportComponent', () => {
  let component: StatreportComponent;
  let fixture: ComponentFixture<StatreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
