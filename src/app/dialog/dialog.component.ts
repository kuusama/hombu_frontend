import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<DialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

}

export enum DialogResult {
  RESULT_OK     = 1,
  RESULT_YES    = 2,
  RESULT_NO     = 3,
  RESULT_CANCEL = 4,
  RESULT_DELETE = 5,
  RESULT_CONFIRM = 6
}