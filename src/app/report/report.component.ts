import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { GetReportCommand, ReportItem, ReportSubitem, ReportTransaction, ReportType } from '../classes/command';
import { CommonReport, IntervalType } from '../common-report';
import { GlobalsService } from '../services/globals.service';
import { NetworkService } from '../services/network.service';
import { MatDialog } from "@angular/material";
import { DateDialogComponent } from '../datedialog/datedialog.component';
import { TransactService } from '../services/transact.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent extends CommonReport implements OnInit {
  tooltip : string = "";
  private refreshRef: Subscription = null;

  constructor(private netService: NetworkService, private globals: GlobalsService,
      translate: TranslateService, private dialog: MatDialog, private transact: TransactService) {
    super(translate);
  }

  ngOnInit() {
    this.doInit();
    this.doRefresh();
    this.tooltip = ( this.globals.getCurrentSettings().showTooltip == 1 ? "tooltip" : "");

    this.refreshRef = this.transact.refresh$.subscribe(()=>{
      this.doRefresh();
    });
  }

  ngOnDestroy() {
    this.refreshRef.unsubscribe();
  }

  public doRefresh() {
    let date_begin = new Date(this.currentDate);
    let date_end = new Date( (this.intervalType == IntervalType.OneDay) ? this.currentDate : this.endDate);
    this.endDate = date_end;

    date_begin.setHours(0,0,0,0);
    //this.beginTimestamp = Math.round( date_begin.getTime()/1000 );
    let offset = date_begin.getTimezoneOffset();
    this.beginTimestamp = Math.round(date_begin.getTime() / 1000) - (60 * offset);

    date_end.setHours(23,59,59,999);
    //this.endTimestamp = Math.round(date_end.getTime()/1000 );
    this.endTimestamp = Math.round(date_end.getTime() / 1000) - (60 * offset);

    Cookie.set('rep_date_begin', date_begin.toDateString(), 10);
    Cookie.set('rep_date_end'  , date_end.toDateString(), 10);
    Cookie.set('rep_date_interval'  , this.intervalType.toString(), 10);

    let comm = new GetReportCommand(this.globals.getCurrentSettings().userid, 
      this.beginTimestamp, this.endTimestamp, ReportType.BASIC_REPORT, 0, 0,
      this.globals.getCurrentSettings().imode);
    
    this.netService.sendCommand(comm).then(result => {
      this.currentReport = JSON.parse(result);
      this.currentDateInterval = ( (this.intervalType == IntervalType.OneDay) ?
              this.formatDate(date_begin) :
              this.formatDate(date_begin) + " - " + this.formatDate(date_end));
    }
    )
  }

  public get_category_report(reportItem : ReportItem) {
    if(0 != reportItem.id) {
      if (!reportItem.subItems){
        let comm = new GetReportCommand(this.globals.getCurrentSettings().userid, 
          this.beginTimestamp, this.endTimestamp, ReportType.ACCOUNT_REPORT, reportItem.id, 0,
          this.globals.getCurrentSettings().imode);
        
          this.netService.sendCommand(comm).then(result => {
            reportItem.subItems = JSON.parse(result);
            reportItem.subItems.forEach(s => {
              s.account = reportItem.id;
              s.style = ( (1 == s.otype) ? 'income' : (2 == s.otype ? 'expense' : 'transfer') );
            });
          });
      } else {
        reportItem.subItems = null;
      }
    }
  }

  public get_transacion_report(subitem : ReportSubitem) {
      if (!subitem.transactions){
        let comm = new GetReportCommand(this.globals.getCurrentSettings().userid, 
          this.beginTimestamp, this.endTimestamp, ReportType.TRANSACTION_REPORT, subitem.account, subitem.id,
          this.globals.getCurrentSettings().imode);
        
          this.netService.sendCommand(comm).then(result => {
            let tmpArr : Array<ReportTransaction> = JSON.parse(result);
            tmpArr.forEach(t => {
              t.otype = 'income';
              
              if (0 == subitem.id) {
                t.otype = 'transfer';
              } else if(t.expense > 0) {
                t.otype = 'expense';
              }

              if (null == t.date) {
                var index = tmpArr.indexOf(t, 0);
                if (index > -1) {
                  tmpArr.splice(index, 1);
                }
              }
            });
            subitem.transactions = tmpArr;
          });
      } else {
        subitem.transactions = null;
      }
  }

  public switchDateSelector() {
    let dialogRef = this.dialog.open(DateDialogComponent, {
      data: {
        dateBegin: this.currentDate,
        dateEnd: this.endDate,
        iType: this.intervalType 
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.currentDate = result.dateBegin;
        this.endDate = result.dateEnd;
        this.intervalType = result.iType;
        this.doRefresh();
      }
    });
  }

  private edit_transaction(trans : ReportTransaction) {
    this.transact.editTransaction(trans.id);
  }
}