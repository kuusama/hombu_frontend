import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TransactionType } from '../classes/transaction';
import { GlobalsService } from '../services/globals.service';
import { PickerComponent } from '../picker/picker.component';
import { TranslateService } from '@ngx-translate/core';
import { DialogComponent, DialogResult } from '../dialog/dialog.component';
import { NetworkService } from '../services/network.service';
import { ProxyService } from '../services/proxy.service';
import { Account } from '../classes/account';
import { Category } from '../classes/category';

enum Operation {
  ADD   = 1,
  SUB   = 2,
  MULT  = 3,
  DIV   = 4,
  SIGN  = 5,
  CLEAR = 6,
  BSP   = 7,
  EQUAL = 8,
  NONE  = 9
}

@Component({
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  Operation = Operation;
  public currentOperation : Operation = Operation.NONE;
  public digitPressed : boolean = false;
  private needToCleanDisplay: boolean = false;
  public regXString: string = "0";
  private regY : number = 0;
  public exRate : number = 1;
  public exResult : number = 0;
  public accounts : Array<Account>;
  public categories : Array<Category>

  constructor(private netService: NetworkService, private dialogRef: MatDialogRef<CalculatorComponent>, 
    @Inject(MAT_DIALOG_DATA) public data, private globals: GlobalsService,
    private translate: TranslateService, private dialog: MatDialog,
    public proxy: ProxyService ) {
      
    }

  ngOnInit() {
    this.proxy.getAccounts().then(accounts => {
      this.accounts = accounts;
      this.proxy.getCategories().then(categories => {
      this.categories = categories;
      this.setupCalculator();
      })
    })
  }

  private setupCalculator() {
    this.regXString = Math.abs(this.data.transaction.amount).toString();
    //Setup accounts if last used accounts is unknown
    var filteredAccounts = this.accounts.filter(x => true);
    if (!this.data.transaction.sourceAccount) {
      this.data.transaction.sourceAccount = filteredAccounts[0].id;
    }

    if (!this.data.transaction.targetAccount && this.data.transaction.type == TransactionType.TRANSFER ) {
      this.data.transaction.targetAccount = filteredAccounts[0].id;
    }
    //Setup category if last used category is unknown
    if (!this.data.transaction.category) {
      this.data.transaction.category = this.categories.filter(x => true)[0].id;
    }
  }

  public doCancel() {
    this.dialogRef.close({result: DialogResult.RESULT_CANCEL});
  }

  private doDeleteTransaction() {
    this.dialogRef.close({result: DialogResult.RESULT_DELETE, trans: this.data.transaction});
  }

  private doConfirmTransaction() {
    let trans = this.data.transaction;
    trans.amount = Number(this.regXString);
    trans.amount *= (this.data.transaction.type == TransactionType.EXPENSE ? -1 : 1);
    trans.amount_exchanged = +(this.data.transaction.amount * this.exRate).toFixed(2);
    this.dialogRef.close({result: DialogResult.RESULT_CONFIRM, trans: this.data.transaction});
  }

  public addDigit(digit: number) {
    this.digitPressed = true;
    let strBefore = (this.needToCleanDisplay ? "0" : this.regXString);
    var strAfter : string = "";
    if ("0" == strBefore) {
      strAfter = (10 == digit ? "0." : digit.toString());
    } else {
      strAfter = strBefore + (10 == digit ? "." : digit.toString());
    }
    this.regXString = strAfter;
    this.data.transaction.amount = Number(this.regXString);
    this.updateExResult();
    this.needToCleanDisplay = false;
  }

  public doOperation(operation: Operation) {
    this.digitPressed = false;
    switch (operation) {
      case Operation.ADD :
      case Operation.SUB :
      case Operation.MULT:
      case Operation.DIV :
        if (0 != this.regY) {
          this.doOperation(Operation.EQUAL);
        }
        this.currentOperation = operation;
        this.needToCleanDisplay = true;
        this.regY = Number(this.regXString);
      break;

      case Operation.SIGN:
        this.data.transaction.amount *=-1;
      break;

      case Operation.CLEAR:
        this.currentOperation = Operation.NONE;
        this.regY = 0;
        this.data.transaction.amount = 0;
      break;

      case Operation.BSP:
        this.regXString = this.regXString.substring(0, this.regXString.length - 1);
        this.data.transaction.amount = Number(this.regXString);
      break;

      case Operation.EQUAL:
        let varX = Number(this.regXString);
        switch (this.currentOperation) {
          case Operation.ADD :
            this.regY += varX;
          break;
          case Operation.SUB :
            this.regY -= varX;
          break;
          case Operation.MULT:
            this.regY *= varX;
          break;
          case Operation.DIV :
            this.regY /= varX;
          break;
        }
        this.currentOperation = Operation.NONE;
        this.data.transaction.amount = this.regY;
        this.regY = 0;
      break;

      case Operation.NONE:
      default:
      break;
    }

    this.regXString = this.data.transaction.amount.toString();
    this.updateExResult();
  }

  /* Keyboard support */
  /* TODO: add operator keys support */
  public keyHandler ($key) {
    if ($key > 47 && $key < 58) {
      this.addDigit(Number(String.fromCharCode($key)));
    } else if ($key == 8) {
      this.doOperation(Operation.BSP);
    } else if ($key == 27) {
      this.doOperation(Operation.CLEAR);
    } else if ($key == 13) {
      this.doOperation(Operation.EQUAL);
    }
    return false;
  }

  private updateExResult() {
    this.exResult = +(this.data.transaction.amount * this.exRate).toFixed(2);
    this.data.transaction.amount_exchanged = this.exResult;
  }

  public exchangeKeyHandler ($key) {
    this.updateExResult();
  }

  private showPicker(data: any) : any {
    let dialogRef = this.dialog.open(PickerComponent, {
      data: data.filter(function( element ) { return element !== undefined; }),
    });
    return dialogRef;
  }

  private showSrcAccPicker() {
    let dialogRef = this.showPicker(this.accounts);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.data.transaction.sourceAccount = result;
        this.globals.setLastUsedSrcAccount(result);
        this.checkTransactionType();
      }
    });
  }

  private showTgtAccPicker() {
    let dialogRef = this.showPicker(this.accounts);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.data.transaction.targetAccount = result;
        this.globals.setLastUsedTgtAccount(result);
        this.checkTransactionType();
      }
    });
  }

  private checkTransactionType() {
    if (TransactionType.TRANSFER == this.data.transaction.type && this.accounts[this.data.transaction.sourceAccount].currency != this.accounts[this.data.transaction.targetAccount].currency) {
      this.data.transaction.type = TransactionType.EXCHANGE;
    } else if (TransactionType.EXCHANGE == this.data.transaction.type && this.accounts[this.data.transaction.sourceAccount].currency == this.accounts[this.data.transaction.targetAccount].currency) {
      this.data.transaction.type = TransactionType.TRANSFER;
      this.exRate = 1;
    }
  }

  private showCatPicker() {
    let dialogRef = this.showPicker(this.categories.filter(cat => Number(cat.otype) == this.data.transaction.type));

    dialogRef.afterClosed().subscribe(result => {
      if (result && 0 != result) {
        this.data.transaction.category = result;
        if (TransactionType.INCOME == this.data.transaction.type) {
          this.globals.setLastUsedIncomeCategory(result);
        } else if (TransactionType.EXPENSE == this.data.transaction.type) {
          this.globals.setLastUsedOutcomeCategory(result);
        }
      }
    });
  }

  private showDialog(text: string) : void {
    let dialogRef = this.dialog.open(DialogComponent, {
      data: { text : text },
    });
  }
}