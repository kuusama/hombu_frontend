import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'picker',
  templateUrl: './picker.component.html',
  styleUrls: ['./picker.component.css']
})
export class PickerComponent implements OnInit {

  public itemCount : number;
  private currentPage : number = 0;
  public  currentMinId : number = 1;
  public  currentMaxId : number = 28;
  public  itemArray: Array<any>;

  constructor(private dialogRef: MatDialogRef<PickerComponent>, 
    @Inject(MAT_DIALOG_DATA) private data : Array<any>) { }

  close() {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.itemCount = this.data.length;
    this.currentMinId = 1;
    this.currentMaxId = this.data.length < 28 ? this.data.length : 28;
    this.refreshPicker();
  }

  public pickerNext() {
    if (this.currentMaxId < this.itemCount) {
        this.currentPage++;
        this.currentMinId = 28 * this.currentPage;
        this.currentMaxId = this.currentMinId + 27;
        if (this.currentMaxId > this.itemCount) {
            this.currentMaxId = this.itemCount;
        } 
        this.refreshPicker();
    }
  }

  public pickerPrev() {
    if (this.currentMinId > 1) {
        this.currentPage--;
        this.currentMinId = 28 * this.currentPage + 1;
        this.currentMaxId = this.currentMinId + 27;
        this.refreshPicker();
    }
  }

  private refreshPicker() {
      this.itemArray = [];
      for(let i = this.currentMinId; i <= (this.itemCount < this.currentMaxId ? this.itemCount 
        : this.currentMaxId); i++) {
          this.itemArray.push(this.data[i-1]);
      }
  }
}