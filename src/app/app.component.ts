import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    public loggedIn: boolean = false;

    ngOnInit() {}

    constructor(private translate: TranslateService) {
        translate.addLangs(['en', 'ru', 'jp']);
        translate.setDefaultLang('en');
        translate.use('en');
    }
}