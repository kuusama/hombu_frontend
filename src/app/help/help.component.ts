import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {
  helpUrl: any;

  constructor(private translate: TranslateService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.translate.get("localeName", {}).subscribe((res: string) => {
      this.helpUrl = "https://litemoney.online/help/help_" + res + ".html";
    });
  }
}