import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ReCaptchaComponent } from 'angular2-recaptcha';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import { LoginCommand, SignInCommand } from '../classes/command';
import { MatDialog } from "@angular/material";
import { DialogComponent } from '../dialog/dialog.component';
import { GlobalsService } from '../services/globals.service';
import { NetworkService } from '../services/network.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Input()  loginState: boolean;
  @Output() loginStateChange = new EventEmitter<boolean>();
  @ViewChild(ReCaptchaComponent, {static: false}) captcha: ReCaptchaComponent;

  ngOnInit() {
    this.doReg = false;
    let cookielist = Cookie.getAll();
    if (null != cookielist) {
      let lang = cookielist.lang;
      let login = cookielist.login;
      let pwd = cookielist.pwd;
      if (null != login && null != pwd) {
        this.login = login;
        this.pwd = pwd;
        this.doLogin();
      }
    }
  }

  private login:       string  = '';
  private pwd:         string  = '';
  private pwd2:        string  = '';
  private email:       string  = '';
  private doReg:       boolean = false;
  private captchaResolved: boolean = false;

  /* Error states */
  private emailError: boolean = false;
  private emailEmpty: boolean = false;
  private passwordEmpty: boolean = false;
  private passwordsNotSame: boolean = false;
  private error: string | null = null;

  constructor(private netService: NetworkService, private globals: GlobalsService,
    private translate : TranslateService, private dialog: MatDialog) { }

    public doCheckPassword(): void {
      this.passwordEmpty = (this.pwd == '');
      //this.checkButton();
    }
  
    private doCheckPassword2(): void {
      this.passwordsNotSame = (this.pwd != this.pwd2);
      //this.checkButton();
    }

  private doCheckEmail(): void {
    this.emailEmpty = (this.email == '');
    this.emailError = !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.email));
    //this.checkButton();
  }

  doRegister(mode: number = 0) {
    if (0 === mode) {
      this.doReg = true;
    } else {
      this.doReg = false;
    }
  }

  doCancel() {
    this.doReset(1);
    this.doReg = false;
  }

  doSubmit(form: NgForm) {
    if(form.valid) {
      this.netService.sendCommand(new SignInCommand(this.login, this.pwd, this.email))
      .then(result => {
        if (result == '0') {
          Cookie.set('login', this.login, 10);
          Cookie.set('pwd', this.pwd, 10);
          this.showDialog("Confirmation message was sent successfully. Check e-mail for additional instructions.");
          this.doReg = false;
        } else {
          this.showDialog("Something went wrong... Please, try again.");
          this.doReg = false;
        }
      }
      )
    }
  }

  handleCaptcha(captchaResponse: string) {
    this.captchaResolved = true;
  }

  handleKey(event) {
    if (13 == event.keyCode) {
      this.doLogin();
    }
  }

  doLogin() {
    if(this.login != '' && this.pwd != '') {
      Cookie.set('login', this.login, 10);
      Cookie.set('pwd', this.pwd, 10);
  
      this.netService.sendCommand(new LoginCommand(this.login, this.pwd))
        .then(result => {
          let res : number = Number(result);
          this.doReset(res);
          if (res > 0) {
            this.globals.downloadUserSettings(res).then(
              prs => {
                this.loginStateChange.emit(res > 0);
              }
            );
          }
        }
        )
    }
  }

  doReset(_loginState: number = 0) {
    this.login = '';
    this.pwd = '';
    this.pwd2 = '';
    this.email = '';
    if (_loginState <= 0) {
      this.showDialog(_loginState == 0 ? "login_failed" :"not_activated");
    }
    this.captchaResolved = false;
    if (this.captcha) {
      this.captcha.reset();
    }
  }

  private showDialog(text: string) : void {
    let dialogRef = this.dialog.open(DialogComponent, {
      data: { text : text },
    });
  }
}
