import { Component, OnInit } from '@angular/core';

import { Account } from '../classes/account';
import { Currency } from '../classes/currency';
import { MatDialog, MatDialogRef } from "@angular/material";
import { DialogComponent } from '../dialog/dialog.component';
import { EditDialogComponent } from '../editdialog/editdialog.component';
import { GlobalsService } from '../services/globals.service';
import { ProxyService } from '../services/proxy.service';
import { NetworkService } from '../services/network.service';

@Component({
  selector: 'accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {
  public accounts: Array<Account>;
  private currencies: Array<Currency>

  private showDialog(text: string, showYes: boolean = false, showNo: boolean = false, showCancel: boolean = false) : MatDialogRef<any> {
    let dialogRef = this.dialog.open(DialogComponent, {
        data: { text : text, show_yes: showYes, show_no: showNo, show_cancel: showCancel },
    });

      return dialogRef;
  }

  constructor(private netService: NetworkService, private globals: GlobalsService,
    private proxy: ProxyService, private dialog: MatDialog) { }

  ngOnInit() {
    this.getCurrencyList();
    this.getAccounts();
  }

  private getAccounts() {
    this.proxy.getAccounts().then(
      accounts => {
        this.accounts = accounts.filter(x => true);
      }
    );
  }

  private getCurrencyList() {
    this.proxy.getCurrencyList().then( result => {
      this.currencies = result;
    });
  }

  private editAccount(event, acnt: Account) {
    let dialogRef = this.dialog.open(EditDialogComponent, {
      data: { object : acnt, currencies: this.currencies, mode: 'edit' },
    });

    dialogRef.afterClosed().subscribe(result => {
      switch (result.action) {
        case 'save':
          this.saveAccount(result.object);
        break;
  
        case 'delete':
          this.deleteAccount(result.object);
        break;
      
        case 'cancel':
        default:
        break;
      };
    });
  }

  private deleteAccount(account: Account) {
    this.proxy.deleteAccount(account).then(result => {
      if (result) {
        this.getAccounts();
      } else {
        this.showDialog("error_delete_account");
      }
    })
  }

  public createAccount() {
    let account = new Account();
    let dialogRef = this.dialog.open(EditDialogComponent, {
      data: { object : account, currencies: this.currencies, mode: 'create' },
    });

    dialogRef.afterClosed().subscribe(result => {
      if ('create' == result.action) {
          let account = result.object;
          this.proxy.createAccount(account).then(result => {
            if (result) {
              this.getAccounts();
            } else {
              this.showDialog("error_creating_account");
            }
          });
      };
    });
  }

  private saveAccount(account: Account) {
    this.proxy.saveAccount(account).then(result => {
      if (result) {
        this.getAccounts();
      } else {
        this.showDialog("error_saving_account");
      }
    }
    );
  }
}
