import { Account } from './account';
import { Category } from './category';
import { SSettings } from './settings';
import { Transaction, TransactionType } from './transaction';

export enum CommandType {
    LOGIN            = 1,
    LOGOUT           = 2,
    PLAY             = 3,
    STOP             = 4,
    SET_SETTING      = 5, //Reserved
    GET_SETTING      = 6, //Reserved
    SET_ALL_SETTINGS = 7,
    GET_ALL_SETTINGS = 8,

    GET_ACCOUNT_LIST = 9,
    GET_CURRENCY_LIST = 10,
    SAVE_ACCOUNT      = 11,
    DELETE_ACCOUNT    = 12,
    CREATE_ACCOUNT    = 13,

    GET_CATEGORY_LIST = 14,
    SAVE_CATEGORY     = 15,
    DELETE_CATEGORY   = 16,
    CREATE_CATEGORY   = 17,
    GET_TYPE_LIST     = 18,

    CONFIRM_TRANSACTION = 19,
    EDIT_TRANSACTION    = 21,
    DELETE_TRANSACTION = 22,

    GET_REPORT          = 20,

    SIGN_IN             = 23,
    NOTIFY_ME           = 24
};

export class ReportItem {
    public id      : number;
    public account : string;
    public begin   : number;
    public income  : number;
    public expense : number;
    public end     : number;
    public subItems: Array<ReportSubitem>;
}

export class ReportSubitem {
    public id      : number;
    public name    : string;
    public income  : number;
    public expense : number;
    public account : number;
    public style   : string;
    public otype   : number;
    public transactions : Array<ReportTransaction>;
}

export class ReportTransaction {
    public id      : number;
    public date    : any;
    public income  : number;
    public expense : number;
    public otype   : string;
    public comment : string;
}

export enum ReportType {
    BASIC_REPORT        = 1,
    ACCOUNT_REPORT      = 2,
    TRANSACTION_REPORT  = 3,
    PIE_ACCOUNT         = 4,
    PIE_CATEGORY        = 5,
    BALANCES_COMBINED   = 6,
    BALANCES_DETAIL     = 7,
    INCOME_DETAIL       = 8,
    EXPENSE_DETAIL      = 9,
    INCOME_COMBINED     = 10,
    EXPENSE_COMBINED    = 11,
} 

export abstract class Command {
    type : CommandType;

    constructor(_type: CommandType) {
        this.type = _type;
    }
}

export class LoginCommand extends Command {
    private login: string;
    private password: string;

    constructor(_login: string, _password: string) {
        super(CommandType.LOGIN);
        this.login = _login;
        this.password = _password;
    }
}

export class SignInCommand extends Command {
    private login: string;
    private password: string;
    private email: string;

    constructor(_login: string, _password: string, _email: string) {
        super(CommandType.SIGN_IN);
        this.login = _login;
        this.password = _password;
        this.email = _email;
    }
}

export class NotifyMeCommand extends Command {
    private email: string;

    constructor(_email: string) {
        super(CommandType.NOTIFY_ME);
        this.email = _email;
    }
}

export class GetAccountsCommand extends Command {
    private user_id: number;
    
    constructor(_user_id: number) {
        super(CommandType.GET_ACCOUNT_LIST);
        this.user_id = _user_id;
    }
}

export class SaveAccountCommand extends Command {
    private user_id: number;
    private account: Account;
    
    constructor(_user_id: number, _account: Account) {
        super(CommandType.SAVE_ACCOUNT);
        this.user_id = _user_id;
        this.account = _account;
    }
}

export class DeleteAccountCommand extends Command {
    private user_id: number;
    private account: number;
    
    constructor(_user_id: number, _account: Account) {
        super(CommandType.DELETE_ACCOUNT);
        this.user_id = _user_id;
        this.account = _account.id;
    }
}

export class CreateAccountCommand extends Command {
    private user_id: number;
    private account: Account;
    
    constructor(_user_id: number, _account: Account) {
        super(CommandType.CREATE_ACCOUNT);
        this.user_id = _user_id;
        this.account = _account;
    }
}

export class ConfirmTransactionCommand extends Command {
    private user_id: number;
    private transaction: Transaction;
    
    constructor(_user_id: number, _transaction: Transaction) {
        super(CommandType.CONFIRM_TRANSACTION);
        this.user_id = _user_id;
        this.transaction = _transaction;
        this.transaction.date = new Date (this.transaction.date);
        let offset = this.transaction.date.getTimezoneOffset();
        this.transaction.date = Math.round(this.transaction.date.getTime() / 1000) - (60 * offset);
    }
}

export class EditTransactionCommand extends Command {
    private user_id: number;
    private trans_id: number;
    
    constructor(_user_id: number, _transaction: number) {
        super(CommandType.EDIT_TRANSACTION);
        this.user_id = _user_id;
        this.trans_id = _transaction;
    }
}

export class DeleteTransactionCommand extends Command {
    private user_id: number;
    private transaction: Transaction;
    
    constructor(_user_id: number, _transaction: Transaction) {
        super(CommandType.DELETE_TRANSACTION);
        this.user_id = _user_id;
        this.transaction = _transaction;
    }
}

export class GetReportCommand extends Command {
    private user_id: number;
    private from: number;
    private to  : number;
    private reptype: ReportType;
    private account: number;
    private category: number;
    private itype   : number;

    constructor(_user_id: number, _from: number, _to: number, _type: ReportType, _account: number, _category: number = 0, 
    _itype: number = 1 ) {
        super(CommandType.GET_REPORT);
        this.user_id = _user_id;
        this.from    = _from;
        this.to      = _to;
        this.reptype = _type;
        this.account = _account;
        this.category = _category;
        this.itype   = _itype;
    }
}

export class GetCategoriesCommand extends Command {
    private user_id: number;
    public cat_type: TransactionType;

    constructor(_user_id: number) {
        super(CommandType.GET_CATEGORY_LIST);
        this.user_id = _user_id;
    }
}

export class SaveCategoryCommand extends Command {
    private user_id: number;
    private category: Category;
    
    constructor(_user_id: number, _category: Category) {
        super(CommandType.SAVE_CATEGORY);
        this.user_id = _user_id;
        this.category = _category;
    }
}

export class DeleteCategoryCommand extends Command {
    private user_id: number;
    private category: number;
    
    constructor(_user_id: number, _category: Category) {
        super(CommandType.DELETE_CATEGORY);
        this.user_id = _user_id;
        this.category = _category.id;
    }
}

export class CreateCategoryCommand extends Command {
    private user_id: number;
    private category: Category;
    
    constructor(_user_id: number, _category: Category) {
        super(CommandType.CREATE_CATEGORY);
        this.user_id = _user_id;
        this.category = _category;
    }
}

export class GetCurrencyListCommand extends Command {
    private user_id: number;
    
    constructor(_user_id: number) {
        super(CommandType.GET_CURRENCY_LIST);
        this.user_id = _user_id;
    }
}

export class GetTypeListCommand extends Command {
    private user_id: number;
    
    constructor(_user_id: number) {
        super(CommandType.GET_TYPE_LIST);
        this.user_id = _user_id;
    }
}

export class GetSettingsCommand extends Command {
    private user_id: number;

    constructor(_user_id: number) {
        super(CommandType.GET_ALL_SETTINGS);
        this.user_id = _user_id;
    }
}

export class SetSettingsCommand extends Command {
    private settings: SSettings;

    constructor(_settings: SSettings) {
        super(CommandType.SET_ALL_SETTINGS);
        this.settings = _settings;
    }
}
