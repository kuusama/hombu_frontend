export class Category {
    public id: number;
    public name: string;
    public icon: number;
    public otype: number;

    constructor() {
        this.icon = 1;
        this.name = "New category";
        this.id = 0;
        this.otype = 1;
    }
}
