export class Currency {
    public id: number;
    public name: string;
    public sign: string;

    public toString() : string {
        return this.name + "(" + this.sign + ")";
    }
}
