export enum Language {
    English  = 1,
    Russian  = 2,
    Japanese = 3
}

export enum Mode {
    Detail  = 1,
    Simple  = 2,
    Piechart = 3,
    Statistic = 4
}

export enum Tip {
    Yes = 1,
    No  = 2
}

export enum Legend {
    Captions  = 1,
    Icons     = 2
}

export enum UseDate {
    CurrentDate  = 1,
    LastUsedDate = 2
}

export class SSettings {
    public userid: number;
    public language: Language;
    public imode   : Mode;
    public lastUsedSrcAccount : number = 0;
    public lastUsedTgtAccount : number = 0;
    public lastUsedIncomeCategory   : number = 0;
    public lastUsedOutcomeCategory  : number = 0;
    public showTooltip              : Tip = Tip.Yes;
    public legendType               : Legend = Legend.Captions;
    public useDate                  : UseDate = UseDate.LastUsedDate;
    public defaultCurrency          : number = 0;

    constructor(_userid : number) {
        this.userid = _userid;
        this.language = Language.English;
        this.imode = Mode.Detail;
        this.showTooltip = Tip.Yes;
        this.lastUsedIncomeCategory = 0;
        this.lastUsedOutcomeCategory = 0;
        this.lastUsedSrcAccount = 0;
        this.lastUsedTgtAccount = 0;
        this.useDate = UseDate.LastUsedDate;
        this.defaultCurrency = 0;
    }
}