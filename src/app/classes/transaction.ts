export enum TransactionType {
    INCOME   = 1,
    EXPENSE  = 2,
    TRANSFER = 3,
    EXCHANGE = 4,
    ALL      = 0
}

export class Transaction {
    public id           : number = 0;
    public date         : any;
    public type         : TransactionType;
    public sourceAccount: number;
    public targetAccount: number;
    public category     : number;
    public amount       : number;
    public amount_exchanged: number;
    public comment      : string;

    constructor (_type: TransactionType) {
        this.type = _type;
        this.targetAccount = 0;
        this.category = 0;
        this.amount = 0;
        this.amount_exchanged = 0;
        this.date = new Date();
        this.comment = "";
    }
}
