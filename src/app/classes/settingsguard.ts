import { Injectable } from '@angular/core';
import { Router, CanActivate , ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import { GlobalsService } from '../services/globals.service';
import { Mode } from './settings';


@Injectable()
export class SettingsGuard implements CanActivate  {
    constructor(private router: Router, public globals: GlobalsService) { }

    canActivate() : boolean {
        let settings = this.globals.getCurrentSettings()
        if (settings) {
            let defaultMode : Mode = settings.imode;

            if (defaultMode != Mode.Detail && defaultMode != Mode.Simple) {
                let path: String = (defaultMode == Mode.Piechart ? "pie" : "stat");
                this.router.navigate([path]);
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}