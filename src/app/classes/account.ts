export class Account {
    public id: number;
    public name: string;
    public currency: number;
    public icon: number;
    public curr_name: string;
    public curr_sign: string;

    constructor() {
        this.icon = 1;
        this.name = "New account";
        this.currency = 1;
        this.id = 0;
    }
}
