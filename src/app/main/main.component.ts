import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { SSettings } from '../../app/classes/settings';
import { GlobalsService } from '../services/globals.service';
import { NetworkService } from '../services/network.service';
import { TransactService } from '../services/transact.service';
import { Router } from "@angular/router";
import { Mode } from '../classes/settings';

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches)
  );

  @Input()  loginState: boolean;
  @Output() loginStateChange = new EventEmitter<boolean>();
  @Output() changeLangEv = new EventEmitter<string>();

  tooltip : string = "";
  public over : string = "over";

  private langLiteral: Array<string> = ['dummy', 'en', 'ru', 'jp'];

  constructor(private router: Router, private breakpointObserver: BreakpointObserver,
    private translate: TranslateService, private netService: NetworkService,
    private globals: GlobalsService, private transact: TransactService) { }

  doLogout() {
    Cookie.delete('login');
    Cookie.delete('pwd');
    this.loginStateChange.emit(false);
  }

changeTip() {
  this.tooltip = ( this.globals.getCurrentSettings().showTooltip == 1 ? "tooltip" : "");
}

newTransaction(type: number) {
  this.transact.newTransaction(type);
}

setMode(mode: number) {
    this.globals.getCurrentSettings().imode = mode;
}

enableSettings(settings: SSettings) {
    this.translate.use(this.langLiteral[settings.language]);
    this.tooltip = (settings.showTooltip == 1 ? "tooltip" : "");
    
    let defaultMode : Mode = settings.imode;

    if (defaultMode != Mode.Detail && defaultMode != Mode.Simple) {
        let path: String = (defaultMode == Mode.Piechart ? "pie" : "stat");
        this.router.navigate([path]);
    }
}

ngOnInit() {
    this.enableSettings(this.globals.getCurrentSettings());
}

}