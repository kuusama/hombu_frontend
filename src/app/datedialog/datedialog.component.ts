import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IntervalType, TimeIntervalType } from '../common-report';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  templateUrl: './datedialog.component.html',
  styleUrls: ['./datedialog.component.css'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-US'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
})
export class DateDialogComponent implements OnInit {
  IntervalType = IntervalType;
  IntervalTypeValues = Object.values(IntervalType).filter(e => typeof(e)=="number");

  TimeIntervalType = TimeIntervalType;
  TimeIntervalTypeValues = Object.values(TimeIntervalType).filter(e => typeof(e)=="number");

  constructor(private dialogRef: MatDialogRef<DateDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data, private adapter: DateAdapter<any>, private translate : TranslateService) { }

  ngOnInit() {
    this.translate.get("localeName", {}).subscribe((res: string) => {
      this.adapter.setLocale(res);
    });
  }

  close() {
    this.dialogRef.close();
  }

}