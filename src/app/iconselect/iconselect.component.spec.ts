import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconselectComponent } from './iconselect.component';

describe('IconselectComponent', () => {
  let component: IconselectComponent;
  let fixture: ComponentFixture<IconselectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconselectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconselectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
