import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'iconselect',
  templateUrl: './iconselect.component.html',
  styleUrls: ['./iconselect.component.css']
})
export class IconselectComponent implements OnInit {

  private iconCount : number = 148;
  public  currentIconPage : number = 0;
  public  currentMinIconId : number = 1;
  private currentMaxIconId : number = 28;
  public  iconIdArray: Array<number>;

  constructor(private dialogRef: MatDialogRef<IconselectComponent>, 
    @Inject(MAT_DIALOG_DATA) private data) { }

  close() {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.currentMinIconId = 1;
    this.currentMaxIconId = 28;
    this.refreshIconPicker();
  }

  public iconPickerNext() {
    if (this.currentMaxIconId < this.iconCount) {
        this.currentIconPage++;
        this.currentMinIconId = 28 * this.currentIconPage;
        this.currentMaxIconId = this.currentMinIconId + 27;
        if (this.currentMaxIconId > this.iconCount) {
            this.currentMaxIconId = this.iconCount;
        } 
        this.refreshIconPicker();
    }
  }

  public iconPickerPrev() {
    if (this.currentMinIconId > 1) {
        this.currentIconPage--;
        this.currentMinIconId = 28 * this.currentIconPage + 1;
        this.currentMaxIconId = this.currentMinIconId + 27;
        this.refreshIconPicker();
    }
  }

  private refreshIconPicker() {
      this.iconIdArray = [];
      for(let i = this.currentMinIconId; i <= (this.iconCount < this.currentMaxIconId ? this.iconCount : this.currentMaxIconId); i++) {
          this.iconIdArray.push(i);
      }
  }
}
