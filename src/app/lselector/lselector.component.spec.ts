import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LselectorComponent } from './lselector.component';

describe('LselectorComponent', () => {
  let component: LselectorComponent;
  let fixture: ComponentFixture<LselectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LselectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LselectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
