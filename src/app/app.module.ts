import { Routes, RouterModule }  from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ReCaptchaModule } from 'angular2-recaptcha';
import { ChartsModule } from 'ng2-charts';
import { UiSwitchModule } from 'ng2-ui-switch';

import { SettingsGuard } from './classes/settingsguard';
import { AccountsComponent } from './accounts/accounts.component';
import { AppComponent } from './app.component';
import { DialogComponent } from './dialog/dialog.component';
import { DateDialogComponent } from './datedialog/datedialog.component';
import { EditDialogComponent } from './editdialog/editdialog.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { EqualValidator } from './equal-validator.directive';
import { GlobalsService } from './services/globals.service';
import { ProxyService } from './services/proxy.service';
import { TransactService } from './services/transact.service';
import { GroupsComponent } from './groups/groups.component';
import { HelpComponent } from './help/help.component';
import { IconselectComponent } from './iconselect/iconselect.component';
import { PickerComponent } from './picker/picker.component';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { NetworkService } from './services/network.service';
import { PiereportComponent } from './piereport/piereport.component';
import { ReportComponent } from './report/report.component';
import { SafePipe } from './classes/safepipe';
import { SettingsComponent } from './settings/settings.component';
import { SetWrapperComponent } from './setwrapper/setwrapper.component';
import { StatreportComponent } from './statreport/statreport.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule,
  MatSelectModule, MatListModule,
  MatInputModule, MatCardModule, MatDialogModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material';
import { MAT_DATE_LOCALE } from '@angular/material/core';

import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import { LswitcherComponent } from './lswitcher/lswitcher.component';

// the second parameter 'fr' is optional
registerLocaleData(localeRu, 'ru');

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

const appRoutes: Routes = [
  { path: '', canActivate: [SettingsGuard], component: ReportComponent, pathMatch: 'full' },
  { path: 'report',   component: ReportComponent },
  { path: 'pie',      component: PiereportComponent },
  { path: 'stat',     component: StatreportComponent },
  { path: 'settings', component: SetWrapperComponent },
  { path: 'help',     component: HelpComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SafePipe,
    MainComponent,
    SetWrapperComponent,
    SettingsComponent,
    AccountsComponent,
    GroupsComponent,
    ReportComponent,
    IconselectComponent,
    PickerComponent,
    PiereportComponent,
    StatreportComponent,
    EqualValidator,
    DialogComponent,
    EditDialogComponent,
    DateDialogComponent,
    CalculatorComponent,
    HelpComponent,
    LswitcherComponent,
  ],
  entryComponents: [
    DialogComponent, 
    IconselectComponent,
    PickerComponent,
    EditDialogComponent,
    DateDialogComponent,
    CalculatorComponent
  ],
  imports: [
    BrowserModule,
    ReCaptchaModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ChartsModule,
    UiSwitchModule,
    MatSelectModule,
    MatTabsModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatCardModule,
    MatDialogModule,
    MatNativeDateModule,
    MatDatepickerModule,
    RouterModule.forRoot(appRoutes),
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    }),
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    BrowserAnimationsModule
  ],
  providers: [SettingsGuard, NetworkService, GlobalsService, TransactService, ProxyService,
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}],
  bootstrap: [AppComponent],
  schemas:   [NO_ERRORS_SCHEMA],
})
export class AppModule { }
