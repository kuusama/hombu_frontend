import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetWrapperComponent } from './setwrapper.component';

describe('SettingsComponent', () => {
  let component: SetWrapperComponent;
  let fixture: ComponentFixture<SetWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
