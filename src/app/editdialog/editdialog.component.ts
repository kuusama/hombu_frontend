import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IconselectComponent } from '../iconselect/iconselect.component';

@Component({
  templateUrl: './editdialog.component.html',
  styleUrls: ['./editdialog.component.css']
})
export class EditDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<EditDialogComponent>, private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
  }

  close() {
    this.dialogRef.close();
  }

  public pickIcon() {
    let dialogRef = this.dialog.open(IconselectComponent, {
      data: {},
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && 0 != result) {
        this.data.object.icon = result;
      }
    });
  }

}