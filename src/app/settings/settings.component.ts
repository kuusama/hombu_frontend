import { Component, EventEmitter, OnInit, Output, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { Language, Legend, Mode, SSettings, Tip, UseDate } from '../../app/classes/settings';
import { Currency } from '../classes/currency';
import { MatDialog } from "@angular/material";
import { DialogComponent } from '../dialog/dialog.component';
import { GlobalsService } from '../services/globals.service';
import { NetworkService } from '../services/network.service';
import { ProxyService } from '../services/proxy.service';

@Component({
    selector: 'settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit, OnDestroy {
    @Output() changeLangEv = new EventEmitter<string>();
    @Output() changeModeEv = new EventEmitter<string>();
    @Output() changeTipEv  = new EventEmitter<string>();
    @Output() changeLegendEv   = new EventEmitter<string>();
    @Output() changeUseDateEv  = new EventEmitter<string>();

    public button_class: string[] = ["button_active", "button", "button"];
    private langLiteral: Array<string> = ['dummy', 'en', 'ru', 'jp'];
    private modeLiteral: Array<string> = ['dummy', 'detail', 'simple', 'piechart', 'statistic'];
    private currencies: Array<Currency> = [];

    languageKeys: Array<any>;   languages = Language;
    modeKeys:     Array<any>;   modes     = Mode;
    tipKeys:      Array<any>;   tips      = Tip;
    legendKeys:   Array<any>;   legends   = Legend;
    useDateKeys:  Array<any>;   usedates  = UseDate;

    public currentSettings: SSettings;

    constructor(private netService: NetworkService, private globals: GlobalsService,
        private translate: TranslateService, private dialog: MatDialog,  private proxy: ProxyService) {
        this.languageKeys  = Object.keys(this.languages).filter(Number);
        this.modeKeys      = Object.keys(this.modes).filter(Number);
        this.tipKeys       = Object.keys(this.tips).filter(Number);
        this.legendKeys    = Object.keys(this.legends).filter(Number);
        this.useDateKeys   = Object.keys(this.usedates).filter(Number);
        this.useDateKeys   = Object.keys(this.usedates).filter(Number);
        this.getCurrencyList();
        this.currentSettings = this.globals.getCurrentSettings();
    }

    private getCurrencyList() {
        this.proxy.getCurrencyList().then( result => {
            this.currencies = result;
        });
    }

    private showDialog(text: string) : void {
        let dialogRef = this.dialog.open(DialogComponent, {
            data: { text : text },
        });
    }

    ngOnDestroy(): void {
        this.saveSettings();
    }

    private writeToGlobalSettings() {
        this.globals.setCurrentSettings(this.currentSettings);
    }

    changeSelectedLang(lang: Language) {
        this.currentSettings.language = lang;
        this.writeToGlobalSettings();
        this.translate.use(this.langLiteral[lang]);
    }

    changeSelectedMode(mode: Mode) {
        this.currentSettings.imode = mode;
        this.writeToGlobalSettings();
        this.changeModeEv.emit(this.modeLiteral[mode]);
    }

    changeSelectedTip(mode: Tip) {
        this.currentSettings.showTooltip = mode;
        this.writeToGlobalSettings();
        this.changeTipEv.emit("1");
    }

    changeSelectedLegend(mode: Legend) {
        this.currentSettings.legendType = mode;
        this.writeToGlobalSettings();
        this.changeLegendEv.emit("1");
    }

    changeSelectedUseDate(mode: UseDate) {
        this.currentSettings.useDate = mode;
        this.writeToGlobalSettings();
        this.changeUseDateEv.emit("1");
    }

    changeSelectedCurrency(curr: number) {
        this.currentSettings.defaultCurrency = curr;
        this.writeToGlobalSettings();
        // this.globals.downloadExchangeRates().then( (res: any) =>
        //     console.log(res['USD'])
        // )
    }

    ngOnInit() {}

    saveSettings() {
        this.globals.uploadUserSettings().then (
            res => {
                if ('1' != res) {
                    this.showDialog("error_saving_settings");
                }
            }
        )
    }
}